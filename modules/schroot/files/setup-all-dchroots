#!/bin/bash

##
## THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
## USE: git clone git+ssh://$USER@puppet.debian.org/srv/puppet.debian.org/git/dsa-puppet.git
##

# Copyright (c) 2013 Peter Palfrader
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

DPKGARCH=$(dpkg --print-architecture)
UNAMEARCH=$(uname -m)

usage() {
    echo >&2 "$0 [--config-only] <buildd|porterbox>"
    exit 1
}

CONFIG_ONLY=no

if [ "${1:-}" = "--config-only" ]; then
    CONFIG_ONLY=yes
    shift
fi

if [ $# -ne 1 ]; then
    usage
elif [ "${1:-}" = "buildd" ]; then
    MODE=buildd
    if ! [ -d /srv/buildd/ ]; then
        echo >&2 "Error: /srv/buildd does not exist or is not a directory."
        exit 1
    fi
elif [ "${1:-}" = "porterbox" ]; then
    MODE=porterbox
else
    echo >&2 "Error: Unknown mode $1"
    exit 1
fi

get_suites() {
    case "$1" in
      amd64|i386|armel|armhf)
        echo "sid stretch jessie wheezy"
        ;;
      mips64el)
        echo "sid stretch"
        ;;
      alpha|hppa|ia64|kfreebsd-*|loong64|m68k|powerpc|powerpcspe|ppc64|sh4|sparc64|x32)
        echo "sid"
        ;;
      *)
        echo "sid stretch jessie"
        ;;
    esac
}

case `hostname -s` in
    alpha-*)
	DPKGARCH=alpha
	;;
    kapitsa)
	DPKGARCH=ppc64
	;;
    loongson)
	DPKGARCH=loong64
	;;
    vs90|vs91|vs92|vs93|m68k-*)
	DPKGARCH=m68k
	;;
    paq|pad|pasta|pacific|paladin)
	DPKGARCH=hppa
	;;
    powerpcspe-*)
	DPKGARCH=powerpcspe
	;;
    vs94|vs95|vs96|vs97|sh4-*)
	DPKGARCH=sh4
	;;
    vs76|x32-*)
        DPKGARCH=x32
        ;;
esac

case `hostname` in
    pizzetti)
        archs="ppc64"
        ;;
    blaauw|debian-project-be-1|debian-project-be-2|perotto|kapitsa|redpanda|ookuninushi)
        archs="powerpc ppc64"
        ;;
    kamp|lemon)
        archs="kfreebsd-amd64 kfreebsd-i386"
        ;;
    *)
        archs="$DPKGARCH"
        case "$DPKGARCH" in
            amd64)
                archs="$archs i386"
                ;;
            arm64)
                archs="$archs armhf armel"
                ;;
            armhf)
                if [ "$(uname -m)" = "aarch64" ] ; then
                    archs="$archs arm64"
                fi
                archs="$archs armel"
                ;;
            armel)
                if [ "$(uname -m)" = "armv7l" ] && grep -w vfpv3 -q /proc/cpuinfo ; then
                    archs="$archs armhf"
                fi
                ;;
            loong64)
                if [ "$(uname -m)" = "loongarch64" ] ; then
                archs="$archs loong64"
		fi
                ;;
            mips64el)
                archs="$archs mipsel"
                ;;
            mipsel)
                archs="$archs mips64el"
                ;;
        esac
        ;;
esac

err=0

do_one() {
    local a="$1"; shift
    local s="$1"; shift
    local extra=""
    local extra_buildd=""
    local extra_debootstrap=""

    if [ "$CONFIG_ONLY" != "no" ]; then
        extra="$extra -c"
    fi

    case `hostname` in
        pizzetti)
            extra="$extra -m http://ftp.de.debian.org/debian-ports"
            extra="$extra -k /root/debian-ports-archive-2016.gpg"
            extra="$extra -I debian-ports-archive-keyring"
            ;;
        *)
            case "$a" in
                ia64)
                    if [ "$s" = "sid" ]; then
                        extra="$extra -m http://deb.debian.org/debian-ports"
                        extra="$extra -k /usr/share/keyrings/debian-ports-archive-keyring.gpg"
                        extra="$extra -I debian-ports-archive-keyring"
                        # Remove once libgcc-7-dev depends on libunwind-dev
                        # rather than the provided libunwind8-dev.
                        extra="$extra -I libunwind-dev"
                    else
                        extra="$extra -m http://deb.debian.org/debian"
                    fi
                    case `hostname -s` in
                        lenz|lifshitz|titanium)
                            # Default of 2 users is good
                            extra_buildd="$extra_buildd -r sbuild"
                            ;;
                        iridium)
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;
                        yttrium)
                            extra="$extra -d /srv/chroot/schroot-unpack"
                            ;;
                    esac
                    ;;
                alpha|hppa|kfreebsd-*|loong64|m68k|powerpc|powerpcspe|ppc64|sh4|sparc64|x32)
                    if [ "$a" = "sparc64" -a "$MODE" = "porterbox" ]; then
                        extra="$extra -m http://deb.debian.org/debian-ports"
                    else
                        case `hostname -s` in
                            turris01|turris02)
                                # IPv6-only, so use a fully-v6 mirror.
                                # Fastly's nameservers don't themselves have
                                # AAAA records, despite supporting v6 on their
                                # network, breaking the deb.debian.org ->
                                # cdn-fastly.deb.debian.org HTTP redirect and
                                # thus debootstrap.
                                # ftp.de.debian.org just doesn't have AAAA
                                # records.
                                # Remove once either of these are fixed.
                                extra="$extra -m http://ftp.ports.debian.org/debian-ports"
                                ;;
                            *)
                                extra="$extra -m http://ftp.de.debian.org/debian-ports"
                                ;;
                        esac
                    fi
                    case "$a" in
                        alpha|m68k)
                            extra_debootstrap="$extra_debootstrap --extra-suites unreleased"
                            ;;
                        # freebsd-utils is Essential (both in metadata and in
                        # system requirements) but resides in unreleased.
                        kfreebsd-*)
                            extra_debootstrap="$extra_debootstrap --extra-suites unreleased"
                            ;;
                    esac
                    extra="$extra -k /usr/share/keyrings/debian-ports-archive-keyring.gpg"
                    extra="$extra -I debian-ports-archive-keyring"
                    case `hostname -s` in

                        # +------------------------------------------------+
                        # | alpha                                          |
                        # +------------------------------------------------+

                        alpha-sc-01)
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;

                        alpha-sc-02)
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;

                        alpha-sc-03)
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;

                        alpha-sc-04)
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;

                        electro)
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;

                        imago)
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;

                        tsunami)
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;

                        # +------------------------------------------------+
                        # | hppa                                           |
                        # +------------------------------------------------+

                        parisc)
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;
                        paq|pad|paladin)
                            extra="$extra -e qemu-debootstrap"
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;
                        pasta|pacific)
                            extra="$extra -e qemu-debootstrap"
                            extra_buildd="$extra_buildd -o buildd,buildd2,buildd3,buildd4 -O buildd,buildd2,buildd3,buildd4"
                            ;;
                        panama)
                            extra_buildd="$extra_buildd -o buildd -O buildd -r sbuild"
                            ;;
                        phantom|atlas|sibaris)
                            extra_buildd="$extra_buildd -o buildd,buildd2,buildd3,buildd4 -O buildd,buildd2,buildd3,buildd4 -r sbuild"
                            ;;

                        # +------------------------------------------------+
                        # | kfreebsd                                       |
                        # +------------------------------------------------+

                        kamp)
                            extra_buildd="$extra_buildd -o buildd -O buildd -r sbuild"
                            ;;
                        lemon)
                            extra="$extra -d /srv/chroot/schroot-unpack"
                            ;;

                        # +------------------------------------------------+
                        # | loong64                                        |
                        # +------------------------------------------------+

                        loongson)
                            extra="$extra -e qemu-debootstrap"
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;

                        # +------------------------------------------------+
                        # | m68k                                           |
                        # +------------------------------------------------+

                        mitchy)
                            extra="$extra -d /srv/chroot/schroot-unpack"
                            ;;

                        vs90|vs91|vs92|vs93|m68k-*)
                            extra="$extra -e qemu-debootstrap"
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;

                        # +------------------------------------------------+
                        # | powerpcspe                                     |
                        # +------------------------------------------------+

                        powerpcspe-gandi-*)
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            extra="$extra -e qemu-debootstrap"
                            ;;
                        atlantis|cb30|pathfinder|turris01|turris02)
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;

                        # +------------------------------------------------+
                        # | ppc64                                          |
                        # +------------------------------------------------+

                        blaauw)
                            # Default of 2 users is good
                            ;;
                        debian-project-be-1)
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;
                        debian-project-be-2)
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;
                        perotto)
                            extra="$extra -d /srv/chroot/schroot-unpack"
                            ;;
                        redpanda)
                            extra="$extra -d /srv/chroot/schroot-unpack -g sbuild,Debian,guest,d-i -r sbuild"
                            ;;
                        kapitsa)
                            # Default of 2 users is good
                            ;;
                        ookuninushi)
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;

                        # +------------------------------------------------+
                        # | sh4                                            |
                        # +------------------------------------------------+

                        vs94|vs95|vs96|vs97|sh4-*)
                            extra="$extra -e qemu-debootstrap"
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;

                        tirpitz)
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;

                        # +------------------------------------------------+
                        # | sparc64                                        |
                        # +------------------------------------------------+

                        deb4g|kyoto|stadler)
                            extra="$extra -d /srv/chroot/schroot-unpack -g sbuild,Debian,guest,d-i -r sbuild"
                            ;;
                        notker|sakharov)
                            extra="$extra -d /srv/chroot/schroot-unpack"
                            ;;
                        andi|landau|nvg5120|osaka|sompek|stadler)
                            extra_buildd="$extra_buildd -o buildd,buildd2,buildd3,buildd4 -O buildd,buildd2,buildd3,buildd4"
                            ;;
                        raverin|ravirin)
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;

                        # +------------------------------------------------+
                        # | x32                                            |
                        # +------------------------------------------------+

                        vs76|x32-do-*)
                            extra_buildd="$extra_buildd -o buildd -O buildd"
                            ;;
                    esac
                    ;;
            esac
            ;;
    esac

    case "$MODE" in
        buildd)
            mkdir -p /srv/buildd/unpack
            if ! chronic setup-dchroot -f -a "$a" $extra -D -d '/srv/buildd/unpack' -K $extra_buildd "$s" $extra_debootstrap
            then
                return 1
            fi
            ;;
        porterbox)
            if ! chronic setup-dchroot -f -a "$a" $extra "$s" $extra_debootstrap
            then
                return 1
            fi
            ;;
        *)
            echo >&2 "Invalid mode $MODE"
            exit 1
    esac
    return 0
}

for a in $archs; do
    for s in `get_suites "$a"`; do
        if ! do_one "$a" "$s"; then
            err=1
            echo >&2
            echo >&2 "Error: setting up $s:$a dchroot failed."
            echo >&2
            echo >&2
        fi
    done
done

exit $err

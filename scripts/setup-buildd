#!/bin/bash
set -e
set -u
set -o pipefail

if [ ! -f "$0" ]; then
	# Can't use die yet - setup-common needs to be sourced
	echo >&2 "$0: Cannot be run from PATH"
	exit 1
fi
puppet_root=`readlink -f "\`dirname "$0"\`/../"`

. "$puppet_root"/scripts/setup-common

# +====================================================================+
# | Determine host details                                             |
# +====================================================================+

debian_ports=yes
no_auto_build=
no_build_regex=
apt_http_proxy=
apt_ftp_proxy=
enable_notify_monitor=yes
auto_clean_uploads=yes
stalled_pkg_timeout=600
no_build_regex_qemu_alpha="bamtools|borgbackup|cglm|clevis|cluster|cpl-|elpa|eso-midas|fcitx5|gcc-|gedit|gegl|gensim|glib-networking|glibc|gnome-calls|gnucash|gnupg2|ignition-common3|inkscape|ismrmrd|kitty|krb5-sync|libambix|libayatana-appindicator|libevdev|libffi-platypus-perl|libime|libminc|libosmocore|librist|libsecret|linux|lme4|lnav|lua-rexlib|mathicgb|mptcpd|mumps|netplan.io|network-manager|nlme|ocaml-ctypes|openldap|pandoc|pango1.0|pipewire|plasma-thunderbolt|praat|python-tesserocr|python2.7|r-bioc-|r-cran-|racket|rcpp|rime-|rmatrix|robustbase|rquantlib|ruby3.1|safeclib|samtools|sngrep|socat|suitesparse|tali|tdb|tor|tpm2-abrmd|tseries|tvc|util-linux|xrdp|yosys"
no_build_regex_qemu_hppa="strace|gdb|binutils|glibc|firebird|openssh|libsecret|gtk+3.0|cmake|raft|libsdl2|eztrace|openvswitch"
# packages which fail in the testsuite when built with qemu-linux-user:
no_build_regex_qemu_hppa_testfails="ethtool|wireshark|cryptsetup|fakeroot|fakechroot|network-manager|glib-networking|systemd|grcompiler|otf2|boost.*|vim|rsyslog|perl|openorienteering-mapper|posixsignalmanager|mariadb|openldap|mediascanner2|openssl|libkdumpfile|devscripts|fuse3|gfs2-utils|util-linux"
# packages which should be avoided on slow buildds:
no_build_regex_slow_machine="linux|acl2|webkit|kde|qt|icedove|vtk|kio|maxima|libre|quiterss|openexr|mathicgb|ns3|paraview|urweb|woo|yade|mpqc3|mrpt|haskell-github|gcc|ghc|suitesparse-graphblas|vim|webkit2gtk|matplotlib|gammu|network-manager"
# packages which should be avoided on physical buildds:
no_build_regex_hppa_physical="nheko|liburing"

case "$hostname" in

	# +----------------------------------------------------------------+
	# | alpha                                                          |
	# +----------------------------------------------------------------+

	alpha-sc-01)
		num_users=1
		DEB_BUILD_OPTIONS=""
		suites="sid experimental"
		sign_with=10DECA80663BA8539A8FFB47907807975C5409F6
		no_build_regex="${no_build_regex_qemu_alpha}"
		mailuser=alpha-sc-01
		mailfqdn=buildd.eu
		;;

	alpha-sc-02)
		num_users=1
		DEB_BUILD_OPTIONS=""
		suites="sid experimental"
		sign_with=51CEE4752366C5DF01488118769C1931D9F606D4
		no_build_regex="${no_build_regex_qemu_alpha}"
		mailuser=alpha-sc-02
		mailfqdn=buildd.eu
		;;

	alpha-sc-03)
		num_users=1
		DEB_BUILD_OPTIONS=""
		suites="sid experimental"
		sign_with=FEF2987FD3B905BAF3DAE95D5E87EE43F6DE1FEA
		no_build_regex="${no_build_regex_qemu_alpha}"
		mailuser=alpha-sc-03
		mailfqdn=buildd.eu
		;;

	alpha-sc-04)
		num_users=1
		DEB_BUILD_OPTIONS=""
		suites="sid experimental"
		sign_with=9D27610196842A28F5342BE4F831FF5DC0E0FDD6
		no_build_regex="${no_build_regex_qemu_alpha}"
		mailuser=alpha-sc-04
		mailfqdn=buildd.eu
		;;

	electro)
		num_users=1
		DEB_BUILD_OPTIONS=""
		suites="sid experimental"
		sign_with=16BC8062FB428918BA55EAE2D7CD5E52C9A67BD7
		#mailuser=
		#mailfqdn=
		;;

	imago)
		num_users=1
		DEB_BUILD_OPTIONS=""
		suites="sid experimental"
		sign_with=ABFA23CE
		#mailuser=
		#mailfqdn=
		;;

	tsunami)
		num_users=1
		DEB_BUILD_OPTIONS=""
		suites="sid experimental"
		sign_with=CC27DE42A8F6537AE2578729D0148D371B850CE5
		no_build_regex="ghc|haskell-openglraw|libreoffice|linux|openjdk-.*|pypy.*|vtk9"
		mailuser=tsunami
		mailfqdn=buildd.eu
		;;

	# +----------------------------------------------------------------+
	# | hppa                                                           |
	# +----------------------------------------------------------------+

	c8000)
		num_users=2
		DEB_BUILD_OPTIONS=""
		suites="sid experimental"
		sign_with=1217894AB7175306DDB9C48A5FE3014715929BEC
		no_build_regex="${no_build_regex_hppa_physical}"
		mailfqdn=c8000.parisc-linux.org
		enable_notify_monitor=no
		apt_http_proxy="http://localhost:3142"
		stalled_pkg_timeout=300
		;;
	parisc)
		num_users=1
		DEB_BUILD_OPTIONS="parallel=2"
		suites="sid experimental"
		sign_with=3EEA277652EA47BB51440347F53BB698944B5504
		no_build_regex="${no_build_regex_hppa_physical}|${no_build_regex_slow_machine}"
		mailfqdn=parisc.parisc-linux.org
		apt_http_proxy="http://localhost:3142"
		stalled_pkg_timeout=300
		;;
	panama)
		num_users=1
		DEB_BUILD_OPTIONS=""
		suites="sid experimental"
		sign_with=A21FFF933D1FBE96518E89FA20195B9713E1D008
		no_build_regex="${no_build_regex_hppa_physical}|${no_build_regex_slow_machine}"
		mailfqdn=panama.parisc-linux.org
		apt_http_proxy="http://localhost:3142"
		stalled_pkg_timeout=300
		;;
	pasta)
		num_users=2
		DEB_BUILD_OPTIONS="nobench parallel=8"
		suites="sid experimental"
		sign_with=C60BC37AF65FFDFA073C02B7756C3BFF12A9AAD9
		no_build_regex="${no_build_regex_qemu_hppa}|${no_build_regex_qemu_hppa_testfails}"
		mailfqdn=pasta.parisc-linux.org
		stalled_pkg_timeout=300
		apt_http_proxy="http://localhost:3142"
		;;
	paq)
		num_users=1
		DEB_BUILD_OPTIONS="nobench"
		suites="sid experimental"
		sign_with=97D73EACCA89E4D9FE13B331E032DBF78C22EE60
		no_build_regex="${no_build_regex_qemu_hppa}|${no_build_regex_qemu_hppa_testfails}|${no_build_regex_slow_machine}"
		mailfqdn=paq.parisc-linux.org
		apt_http_proxy="http://localhost:3142"
		;;
	pad)
		num_users=1
		DEB_BUILD_OPTIONS="nobench"
		suites="sid experimental"
		sign_with=54814BECDD77823C5C229F9F8F7A0EA38936C31B
		no_build_regex="${no_build_regex_qemu_hppa}|${no_build_regex_qemu_hppa_testfails}"
		mailfqdn=pad.parisc-linux.org
		apt_http_proxy="http://mx3210:3142"
		;;
	paladin)
		num_users=1
		DEB_BUILD_OPTIONS="nobench"
		suites="sid experimental"
		sign_with=D85259AFC4F50448FDAD39C54033D6C2F16F6DCC
		no_build_regex="${no_build_regex_qemu_hppa}|${no_build_regex_qemu_hppa_testfails}"
		mailfqdn=paladin.parisc-linux.org
		stalled_pkg_timeout=300
		apt_http_proxy="http://localhost:3142"
		;;
	pacific)
		num_users=4
		DEB_BUILD_OPTIONS="nobench parallel=31"
		suites="sid experimental"
		sign_with=E5F4BB185A0099FA1D8821F5C2B245D93E44E408
		no_build_regex="${no_build_regex_qemu_hppa}|${no_build_regex_qemu_hppa_testfails}"
		mailfqdn=pacific.parisc-linux.org
		stalled_pkg_timeout=300
		apt_http_proxy="http://localhost:3142"
		;;
	phantom)
		num_users=4
		DEB_BUILD_OPTIONS=""
		suites="sid experimental"
		sign_with=94695ADDF097CDF0574F29ABB38AEC54B70345B7
		mailfqdn=phantom.parisc-linux.org
		no_build_regex="${no_build_regex_hppa_physical}"
		apt_http_proxy="http://localhost:3142"
		stalled_pkg_timeout=300
		;;
	sibaris)
		num_users=4
		DEB_BUILD_OPTIONS="parallel=4"
		suites="sid experimental"
		sign_with=BABC24CAA4E2EC67A8B5A75CF77DBE9F614CB9FB
		no_build_regex="${no_build_regex_hppa_physical}"
		mailfqdn=sibaris.parisc-linux.org
		apt_http_proxy="http://localhost:3142"
		stalled_pkg_timeout=300
		;;
	mx3210)
		num_users=1
		DEB_BUILD_OPTIONS="parallel=4"
		suites="sid experimental"
		sign_with=56C650BA5969CDB50B46997131AD3ACF70D0372E
		no_build_regex="${no_build_regex_hppa_physical}"
		mailfqdn=mx3210.parisc-linux.org
		apt_http_proxy="http://localhost:3142"
		stalled_pkg_timeout=300
		;;
	atlas)
		num_users=2
		DEB_BUILD_OPTIONS="parallel=4"
		suites="sid experimental"
		sign_with=E43D3BDC2B1EE26CBA3CA56D4EC51EE98178AE41
		no_build_regex="${no_build_regex_hppa_physical}"
		mailfqdn=atlas.parisc-linux.org
		apt_http_proxy="http://mx3210:3142"
		stalled_pkg_timeout=300
		enable_notify_monitor=no
		;;

	# +----------------------------------------------------------------+
	# | ia64                                                           |
	# +----------------------------------------------------------------+

	iridium)
		num_users=1
		DEB_BUILD_OPTIONS="parallel=2"
		suites="sid experimental"
		sign_with=B1F444182D55E138AEF1E563494DD6BE57787671
		mailuser=iridium
		mailfqdn=buildd.eu
		;;

	lenz)
		num_users=2
		DEB_BUILD_OPTIONS="parallel=2"
		suites="sid experimental"
		sign_with=D576CE9BA3BEACB2CA3D9BC1467D6149C61004DC
		debian_ports=mixed
		apt_http_proxy="http://localhost:3128"
		;;

	lifshitz)
		num_users=2
		DEB_BUILD_OPTIONS="parallel=2"
		suites="sid experimental"
		sign_with=88D81820EBDDFA27F8376B5D792C1CF713782A52
		debian_ports=mixed
		apt_http_proxy="http://localhost:3128"
		;;

	titanium)
		num_users=2
		DEB_BUILD_OPTIONS="parallel=2"
		suites="sid experimental"
		sign_with=3E0910F1226B52A70FAAAD09113DCD1009F76FE4
		debian_ports=mixed
		mailuser=titanium
		mailfqdn=buildd.eu
		apt_http_proxy="http://localhost:3128"
		;;

	# +----------------------------------------------------------------+
	# | kfreebsd                                                       |
	# +----------------------------------------------------------------+

	kamp)
		num_users=1
		DEB_BUILD_OPTIONS="parallel=2"
		# XXX: No experimental; aptitude is uninstallable
		suites="sid"
		sign_with=C009E8FF38EFC456F8A2F303E545C9A99EC9169C
		other_arches=kfreebsd-i386
		stalled_pkg_timeout=150
		# XXX: Seems to hang parts of the system; investigate/revisit with 12
		no_build_regex="anacron"
		;;

	# +----------------------------------------------------------------+
	# | loong64                                                        |
	# +----------------------------------------------------------------+
	#TODO, Please advise buildd	
	loongson)
		num_users=1
		DEB_BUILD_OPTIONS="nobench nocheck"
		suites="sid experimental"
		sign_with=
		no_build_regex=
		mailuser=loongson
		mailfqdn=buildd.eu
		;;

	# +----------------------------------------------------------------+
	# | m68k                                                           |
	# +----------------------------------------------------------------+

	vs90)
		num_users=1
		DEB_BUILD_OPTIONS="nobench nocheck"
		suites="sid experimental"
		sign_with=64709BAE43083B28D16441CE692460F7D80B8BB9
		no_build_regex="^firebird"
		mailuser=vs90
		mailfqdn=buildd.eu
		;;
	vs91)
		num_users=1
		DEB_BUILD_OPTIONS="nobench nocheck"
		suites="sid experimental"
		sign_with=29DCA7361C239C162077EADBA177EA0C4CA19B3B
		no_build_regex="^firebird"
		mailuser=vs91
		mailfqdn=buildd.eu
		;;
	vs92)
		num_users=1
		DEB_BUILD_OPTIONS="nobench nocheck"
		suites="sid experimental"
		sign_with=E0E440A28A2F727E376F963C43C4A52810637764
		no_build_regex="^firebird"
		mailuser=vs92
		mailfqdn=buildd.eu
		;;
	vs93)
		num_users=1
		DEB_BUILD_OPTIONS="nobench nocheck"
		suites="sid experimental"
		sign_with=D59E01CB526F963F555278B70A6D4D54D6B43194
		no_build_regex="^firebird"
		mailuser=vs93
		mailfqdn=buildd.eu
		;;
	m68k-do-01)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck parallel=2"
		suites="sid experimental"
		sign_with=38E7CD0FF85466C0457D94BFDACE3236A3342C6A
		no_build_regex="^firebird"
		;;
	m68k-do-02)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck parallel=2"
		suites="sid experimental"
		sign_with=05EFAF6B2543E69121309B78F91C05D4672D76A8
		no_build_regex="^firebird"
		;;
	m68k-gandi-01)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck parallel=2"
		suites="sid experimental"
		sign_with=D5E40AC12B3AC4CB554123A16314747B8F2C2378
		no_build_regex="^firebird"
		;;
	m68k-gandi-02)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck parallel=2"
		suites="sid experimental"
		sign_with=842A25A1F77E220CFB9480B36A766530AAC3A9CE
		no_build_regex="^firebird"
		;;
	m68k-osuosl-01)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck parallel=2"
		suites="sid experimental"
		sign_with=64EBB40509D174E4C87E161A242B35DC8D72608E
		no_build_regex="^firebird"
		;;

	m68k-osuosl-02)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck parallel=2"
		suites="sid experimental"
		sign_with=61A5946D969AAA19AEB8E82317D5D7364F758829
		no_build_regex="^firebird"
		;;

	# +----------------------------------------------------------------+
	# | powerpcspe                                                     |
	# +----------------------------------------------------------------+

	atlantis)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck parallel=2"
		suites="sid experimental"
		sign_with=D585C6A8FB13B99F844FCEAC6A8DA89BA1270ADC
		no_build_regex="ghc|llvm-toolchain.*"
		mailuser=atlantis
		mailfqdn=buildd.eu
		;;
	cb30)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck"
		suites="sid experimental"
		sign_with=E182E31C84A40AC717DE1397EE4D84DA0CBAEDD0
		no_build_regex="ghc"
		mailuser=cb30
		mailfqdn=buildd.eu
		;;
	pathfinder)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck parallel=2"
		suites="sid experimental"
		sign_with=01B7376918B7A9C6840C42B9B44ED270F22DBDCA
		no_build_regex="ghc|llvm-toolchain.*"
		mailuser=pathfinder
		mailfqdn=buildd.eu
		;;
	powerpcspe-gandi-01)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck parallel=2"
		suites="sid experimental"
		sign_with=E1C337C4AD71B4909409D50E4709768525A44B2B
		no_build_regex="llvm-toolchain.*"
		;;
	powerpcspe-gandi-02)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck parallel=2"
		suites="sid experimental"
		sign_with=8A7FCCB13E4A84B5B5A1934B44C6B3F4E4707D20
		no_build_regex="llvm-toolchain.*"
		;;
	turris01)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck parallel=2"
		suites="sid experimental"
		sign_with=D3EC8E9D65C641119B8135624A7C01A0F008D269
		no_build_regex="ghc|llvm-toolchain.*"
		;;
	turris02)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck parallel=2"
		suites="sid experimental"
		sign_with=DD9CB135E07ED8C4157D8038F0C6F15CA0B31D4F
		no_build_regex="ghc|llvm-toolchain.*"
		;;

	# +----------------------------------------------------------------+
	# | ppc64                                                          |
	# +----------------------------------------------------------------+

	blaauw)
		num_users=2
		DEB_BUILD_OPTIONS="parallel=16"
		suites="sid experimental"
		sign_with=BC65F5C31BA425DD5666DA3F324575930CC03AA6
		other_arches=powerpc
		mailuser=blaauw
		mailfqdn=buildd.eu
		stalled_pkg_timeout=150
		;;

	debian-project-be-1)
		num_users=1
		DEB_BUILD_OPTIONS="parallel=8"
		suites="sid experimental"
		sign_with=D202172A96B3C9F8F1B967FC7FD5439918A35D25
		other_arches=powerpc
		mailuser=debian-project-be-1
		mailfqdn=buildd.eu
		stalled_pkg_timeout=150
		;;

	debian-project-be-2)
		num_users=1
		DEB_BUILD_OPTIONS="parallel=8"
		suites="sid experimental"
		sign_with=EEA481B83BD15A481DBBAB233AC4684E0CF76482
		other_arches=powerpc
		mailuser=debian-project-be-2
		mailfqdn=buildd.eu
		stalled_pkg_timeout=150
		;;

	kapitsa)
		num_users=2
		DEB_BUILD_OPTIONS="parallel=32"
		suites="sid experimental"
		sign_with=B5A0D9AB2B2A19CC5E14ED7CC3B2B7D313319592
		other_arches=powerpc
		apt_http_proxy="http://localhost:3128"
		stalled_pkg_timeout=150
		;;

	ookuninushi)
		num_users=1
		DEB_BUILD_OPTIONS="parallel=16"
		suites="sid experimental"
		sign_with=0E3B6F20FF0A8EBA82DC2B6582CAB2E3F144F163
		other_arches=powerpc
		no_build_regex="golang-1.9|golang-1.10"
		mailuser=ookuninushi
		mailfqdn=buildd.eu
		stalled_pkg_timeout=150
		;;

	# +----------------------------------------------------------------+
	# | sh4                                                            |
	# +----------------------------------------------------------------+

	vs94)
		num_users=1
		DEB_BUILD_OPTIONS="nobench nocheck"
		suites="sid experimental"
		sign_with=CD0DD142C5459D5A59AB32F03CBE650D18E587B5
		no_build_regex="kdelibs4support|qmapshack|pillow|plasma-framework"
		mailuser=vs94
		mailfqdn=buildd.eu
		;;
	vs95)
		num_users=1
		DEB_BUILD_OPTIONS="nobench nocheck"
		suites="sid experimental"
		sign_with=2E35425292D2FF3CBCF3527CD1A308BD7C0AE03C
		no_build_regex="kdelibs4support|qmapshack|pillow|plasma-framework"
		mailuser=vs95
		mailfqdn=buildd.eu
		;;
	vs96)
		num_users=1
		DEB_BUILD_OPTIONS="nobench nocheck"
		suites="sid experimental"
		sign_with=969AA1D2E4A5DEBFAA266E6244EDAD217C92DDAA
		no_build_regex="kdelibs4support|qmapshack|pillow|plasma-framework"
		mailuser=vs96
		mailfqdn=buildd.eu
		;;
	vs97)
		num_users=1
		DEB_BUILD_OPTIONS="nobench nocheck"
		suites="sid experimental"
		sign_with=3417EB64C5E8B60140CEB3B03D01E265DA2C5F12
		no_build_regex="kdelibs4support|qmapshack|pillow|plasma-framework"
		mailuser=vs97
		mailfqdn=buildd.eu
		;;
	tirpitz)
		num_users=1
		DEB_BUILD_OPTIONS="nobench nocheck"
		suites="sid experimental"
		sign_with=A79172BDF7C738971D7FA97C89576BDDB8D228F4
		no_build_regex="ghc|openjdk-.*|gcc-.*|linux|vtk6|qmapshack"
		mailuser=tirpitz
		mailfqdn=buildd.eu
		;;
	sh4-do-01)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck parallel=2"
		suites="sid experimental"
		sign_with=62531518E6828817E43E722D8063B95B39602781
		no_build_regex="kdelibs4support|qmapshack|pillow|plasma-framework"
		;;
	sh4-do-02)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck parallel=2"
		suites="sid experimental"
		sign_with=D74AA2557A811EB4196A004240BF8D6BBFACC7CA
		no_build_regex="kdelibs4support|qmapshack|pillow|plasma-framework"
		;;
	sh4-gandi-01)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck parallel=2"
		suites="sid experimental"
		sign_with=89E945EEE4B62D800241BE5A8072F725183BB84E
		no_build_regex="kdelibs4support|qmapshack|pillow|plasma-framework"
		;;
	sh4-gandi-02)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck parallel=2"
		suites="sid experimental"
		sign_with=6CEDB714F8C754263579981B886FF34512A39B4F
		no_build_regex="kdelibs4support|qmapshack|pillow|plasma-framework"
		;;
	sh4-osuosl-01)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck parallel=2"
		suites="sid experimental"
		sign_with=795D40BEB0FDAA2C820CA0BAE764BAE9E7C17DB8
		no_build_regex="kdelibs4support|qmapshack|pillow|plasma-framework"
		;;

	sh4-osuosl-02)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="nobench nocheck parallel=2"
		suites="sid experimental"
		sign_with=3CB941A35A6F78E8CFC30BBA0086D4B6628E2576
		no_build_regex="kdelibs4support|qmapshack|pillow|plasma-framework"
		;;

	# +----------------------------------------------------------------+
	# | sparc64                                                        |
	# +----------------------------------------------------------------+

	deb4g|notker|sakharov)
		die "$hostname is not a buildd"
		;;
	andi)
		num_users=4
		# 24 cores / 4 users = 6 cores per user
		DEB_BUILD_OPTIONS=parallel=6
		suites="sid experimental"
		sign_with=DE47BB273C61BA7D50C85B91C4ABBC373E424EA1
		no_build_regex="gcc-.*|acl2|linux|ghc|llvm-toolchain-.*|rustc|cargo"
		;;
	landau)
		num_users=4
		# 128 cores / 4 users = 32 cores per user
		DEB_BUILD_OPTIONS=parallel=32
		suites="sid experimental"
		sign_with=651DB39555BF954B17265BCB179141F84D993EE2
		apt_http_proxy="http://localhost:3128"
		no_build_regex="gdb|openjdk-8|openjdk-9|python3.9|python3.10"
		;;
	nvg5120)
		num_users=4
		DEB_BUILD_OPTIONS=parallel=16
		suites="sid experimental"
		sign_with=A17407EB8CA714B0298B8DF0C56226E53AD096B4
		# no_build_regex="gcc-.*|ghc|gnutls28|llvm-toolchain-.*|linux|openjdk-.*|cargo|rustc|mercurial"
		;;
	osaka)
		num_users=4
		DEB_BUILD_OPTIONS=parallel=8
		suites="sid experimental"
		sign_with=9C7091BB1FAC1327BEDFAE15C16F15B82344190A
		no_build_regex="gnutls28|openjdk-.*|mercurial"
		;;
	raverin)
		num_users=1
		# Single-core machine
		DEB_BUILD_OPTIONS=parallel=1
		suites="sid experimental"
		sign_with=5B0086E08CE65ADF9637B4BE08E83B9B4634AD75
		no_build_regex="gcc-.*|acl2|linux|ghc|git|systemd|libreoffice|llvm-toolchain-.*|ceph|qemu|haskell-src-exts|octave|openjdk-*|sumo|connectome-workbench|mysql-workbench|openfoam|qtbase-opensource-src|rocksdb|rustc|cargo|freeorion|cmake"
		;;
	sompek)
		num_users=4
		# 64 cores / 4 users = 16 cores per user
		DEB_BUILD_OPTIONS=parallel=16
		suites="sid experimental"
		sign_with=4376886BFCE4075451E71D0936F5E2928F173F22
		# no_build_regex="gcc-.*|acl2|linux|ghc|gnutls28|llvm-toolchain-.*|openjdk-.*|rustc|cargo|mercurial"
		;;

	# +----------------------------------------------------------------+
	# | x32                                                            |
	# +----------------------------------------------------------------+

	vs76)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="parallel=2"
		suites="sid experimental"
		sign_with=DA248675
		mailuser=vs76
		mailfqdn=buildd.eu
		stalled_pkg_timeout=150
		;;
	x32-do-01)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="parallel=2"
		suites="sid experimental"
		sign_with=9A0E179541182ABB2361A3D20C6753B83593530B
		stalled_pkg_timeout=150
		;;
	x32-do-02)
		num_users=1
		# Dual-core machine
		DEB_BUILD_OPTIONS="parallel=2"
		suites="sid experimental"
		sign_with=F788820ADC17BE1C6E2782135BF86A822C4D5B0E
		stalled_pkg_timeout=150
		;;

	# +----------------------------------------------------------------+
	# | Unknown                                                        |
	# +----------------------------------------------------------------+

	*)
		die "$hostname is an unknown host"
		;;
esac

if [ -z "${mailfqdn:-}" ]; then
	mailfqdn=$fqdn
fi

slashed_arches="$arch"
for a in ${other_arches:-}; do
	slashed_arches="$slashed_arches / $a"
done

logs_mailed_to="logs@buildd.debian.org"
if [ "$sign_with" = "manual" ]; then
	logs_mailed_to="$logs_mailed_to,$admin_mail"
fi

# debsign 2.17.3 is broken for using with sbuild on a buildd (#857964)
devscripts_version=$(dpkg -l devscripts | tail -n 1 | awk '{print $3}')
if [ "$devscripts_version" = "2.17.3" ]; then
	apt-get update
	PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/sbin:/usr/local/bin apt-get install -y devscripts </dev/null
fi

# +====================================================================+
# | Setup schroot                                                      |
# +====================================================================+

ensure_file "/etc/schroot/setup.d/99builddsourceslist" - root root 555 < "$puppet_root/modules/schroot/files/schroot-setup.d/99builddsourceslist"

case "$arch" in
	kfreebsd-*)
		ensure_file "/etc/schroot/setup.d/99devfs" - root root 555 < "$puppet_root/modules/schroot/files/schroot-setup.d/99devfs"
		ensure_file "/etc/schroot/setup.d/99shm" - root root 555 < "$puppet_root/modules/schroot/files/schroot-setup.d/99shm"
	;;
esac

ensure_file "/usr/local/sbin/setup-dchroot" - root root 555 < "$puppet_root/modules/schroot/files/setup-dchroot"
ensure_file "/usr/local/sbin/setup-all-dchroots" - root root 555 < "$puppet_root/modules/schroot/files/setup-all-dchroots"

# Vim syntax highlighting doesn't like putting this in a subshell
# Always-true if-block is equivalent
if true; then
	# lifted from modules/schroot/templates/schroot-buildd/fstab.erb and converted
	# to shell
	cat <<EOF
# fstab: static file system information for chroots.
# Note that the mount point will be prefixed by the chroot path
# (CHROOT_PATH)
#
# <file system>	<mount point>	<type>	<options>	<dump>	<pass>

EOF
	case "$arch" in
		kfreebsd-*)
			cat <<EOF
# kFreeBSD version
proc		/proc		linprocfs	defaults	0	0
sys		/sys		linsysfs	defaults	0	0
dev		/dev		devfs		rw		0	0
dev		/dev/fd		fdescfs		rw		0	0
tmpfs-shm	/run/shm	tmpfs		nosuid,noexec,size=64m,mode=1777	0 0

EOF
			if [ -d "/srv/build-trees" ]; then
				cat <<EOF
/srv/build-trees	/build	nullfs		rw		0	0
EOF
			fi
			;;
		*)
			cat <<EOF
# Linux version
/proc		/proc		none	rw,bind		0	0
/sys		/sys		none	rw,bind		0	0
/dev/pts	/dev/pts	none	rw,bind		0	0
tmpfs-shm	/dev/shm	tmpfs	defaults,size=64m	0 0

EOF
			if [ -d "/srv/build-trees" ]; then
				cat <<EOF
/srv/build-trees	/build	none	rw,bind		0	0

EOF
			fi
		;;
	esac
fi | ensure_file "/etc/schroot/buildd/fstab" - root root

ensure_file "/etc/schroot/buildd/config" "CHROOT_FILE_UNPACK_DIR=/srv/buildd/unpack\n" root root

if true; then
	case "$debian_ports" in
		yes|no)
			cat <<EOF
debian_ports=$debian_ports
EOF
		;;
		mixed)
			cat <<EOF
if [ "\$SUITE_BASE" = "sid" ]; then
	debian_ports=yes
fi
EOF
	esac
	if [ "$debian_ports" != "no" ]; then
		cat <<EOF
debian_unreleased=yes
EOF
	fi
	if [ -n "${apt_http_proxy}" ]; then
		cat <<EOF
apt_http_proxy='${apt_http_proxy}'
EOF
	fi
	if [ -n "${apt_ftp_proxy}" ]; then
		cat <<EOF
apt_ftp_proxy='${apt_ftp_proxy}'
EOF
	fi
fi | ensure_file "/etc/schroot/conf.buildd" - root root

# +====================================================================+
# | Setup sbuild and buildd                                            |
# +====================================================================+

if true; then
	cat <<EOF
##
## THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
## USE: git clone git+ssh://\$USER@puppet.debian.org/srv/puppet.debian.org/git/dsa-puppet.git
##

# ASPCUD optimization criteria for experimental
\$aspcud_criteria = '-count(solution,APT-Release:=/experimental/),-removed,-changed,-new';
EOF
	if [ -n "${DEB_BUILD_OPTIONS:-}" ]; then
		cat <<EOF

\$build_environment = {
	'DEB_BUILD_OPTIONS' => '$DEB_BUILD_OPTIONS'
};
EOF
	fi
fi | ensure_file "/etc/sbuild/sbuild.conf" - root root

if true; then
	cat <<EOF
# Gross hack because /usr/bin/buildd as run by the init script doesn't unset
# environment variables, only buildd-watcher does (run by cron, and is where
# the list here is taken from)

unset -v LANG
unset -v LANGUAGE
unset -v LC_CTYPE
unset -v LC_NUMERIC
unset -v LC_TIME
unset -v LC_COLLATE
unset -v LC_MONETARY
unset -v LC_MESSAGES
unset -v LC_PAPER
unset -v LC_NAME
unset -v LC_ADDRESS
unset -v LC_TELEPHONE
unset -v LC_MEASUREMENT
unset -v LC_IDENTIFICATION
unset -v LC_ALL
# other unneeded variables that might be set
unset -v DISPLAY
unset -v TERM
unset -v XDG_RUNTIME_DIR
unset -v XDG_SEAT
unset -v XDG_SESSION_COOKIE
unset -v XDG_SESSION_ID
unset -v XDG_VTNR
EOF
fi | ensure_file "/etc/default/buildd" - root root

for i in `seq 1 $num_users`; do
	[ $i -eq 1 ] && iopt="" || iopt="$i"
	hopt=$iopt
	# if last char of hostname is numeric use b..z instead 2..26
	if [[ ! -z "$iopt" && "${hostname: -1}" =~ [0-9] ]] ; then
		hopt=$(printf "\x$(printf %x $((96+$i)))")
	fi
	user=buildd$iopt
	err=0 && getent passwd $user >/dev/null || err=$?
	if [ $err -eq 2 ]; then
		die "$user user does not yet exist"
	elif [ $err -ne 0 ]; then
		die "getent passwd $user: exit code $err"
	fi

	userhome=$(eval echo "~$user")
	ensure_directory "$userhome" 2755 buildd "$user"
	ensure_directory "$userhome/build" 2750 buildd "$user"
	ensure_directory "$userhome/logs" 2750 buildd "$user"
	ensure_directory "$userhome/old-logs" 2750 buildd "$user"
	ensure_directory "$userhome/upload-security" 2750 buildd "$user"
	ensure_directory "$userhome/stats" 2755 buildd "$user"
	ensure_directory "$userhome/stats/graphs" 2755 buildd "$user"
	ensure_directory "$userhome/upload" 2755 buildd "$user"
	ensure_file "$userhome/.forward" "|/usr/bin/buildd-mail\n" buildd "$user"
	ensure_directory "$userhome/.gnupg" 700 buildd "$user"
	ensure_file "$userhome/.gnupg/gpg.conf" "personal-digest-preferences SHA512\n" buildd "$user"

	if true; then
		cat <<EOF
#
# example for ~/.sbuildrc
# commented out stuff are defaults
#
# \$Id: example.sbuildrc,v 1.2 2000/03/09 13:13:16 rnhodek Exp $

# Require chrooted building?
#\$chroot_only=1;

\$fakeroot = "fakeroot";

# Directory for writing build logs to
\$log_dir = "\$HOME/logs";

# Mail address where logs are sent to (mandatory, no default!)
# this moved to .builddrc...
\$mailto = '$logs_mailed_to';

\$mailfrom = '${mailuser:-$user}${mailuser:+$iopt}@$mailfqdn';

# Maintainer name to use in .changes files (mandatory, no default!)
\$maintainer_name='$slashed_arches Build Daemon ($hostname) <${mailuser:-$user}${mailuser:+$iopt}@$mailfqdn>';

# When to purge the build directory afterwards; possible values are "never",
# "successful", and "always"
\$purge_build_directory="always";

# After that time (in minutes) of inactivity a build is terminated. Activity
# is measured by output to the log file.
\$stalled_pkg_timeout = $stalled_pkg_timeout;

# Some packages may exceed the general timeout (e.g. redirecting output to
# a file) and need a different timeout. Below are some examples.
#require "/home/buildd/globals/sbuildrc-timeout";

\$sbuild_mode = "buildd";

# No longer the default as of 0.74.0-1 (See: #870263), and buildd doesn't
# override it (See: #893608).
\$build_arch_all = 0;
# Ditto
\$run_lintian = 0;

#%alternatives = (
#       svgalibg1-dev => svgalib-dummyg1,
#       libmotif-dev => lesstif2-dev
#);

\$apt_update = 0;
\$apt_distupgrade = 0;
\$apt_upgrade = 0;
EOF
		if [ "$arch" = "kfreebsd-amd64" ]; then
			cat <<EOF
# Force new glibc:
#  - 2.25-3+kbsd.1: getentropy implementation and sem_open fix
#  - 2.25-3+kbsd.2: i386 malloc alignment, and64 libmvec
\$manual_depends = [ 'libc0.1-dev (>= 2.25-3+kbsd.2)' ];
EOF
		fi
		if [ "$arch" = "m68k" ] || [ "$arch" = "sh4" ]; then
			cat <<EOF
# Force glibc from unreleased on m68k and sh4 for QEMU-related fixes
\$manual_depends = [ 'libc6-dev (>= 2.36-4+ports)' ];
EOF
		fi
		if [ "$arch" = "sparc64" ]; then
			cat <<EOF
# Force glibc from unreleased on sparc64 due to #1020974
\$manual_depends = [ 'libc6-dev (>= 2.35-1+sparc64)' ];
EOF
		fi
		cat <<EOF

# don't remove this, Perl needs it:
1;

EOF
	fi | ensure_file "$userhome/.sbuildrc" - buildd "$user"

	if true; then
		if [ "$debian_ports" != "no" ]; then
			cat <<EOF
package config;

\$default_host = "dports";

\$cfg{'dports'} = {
	fqdn => "ports-master.debian.org",
	incoming => "/incoming/",
	visibleuser => "${mailuser:-$user}${mailuser:+$iopt}",
	visiblename => "$mailfqdn",
	fullname => "$slashed_arches Build Daemon ($hostname)",
	passive => 1,
	dinstall_runs => 1,
};
EOF
		else
			cat <<EOF
package config;

\$default_host = "ftp-master";

\$cfg{'ftp-master'} = {
	fqdn => "ftp.upload.debian.org",
	incoming => "/pub/UploadQueue/",
	visibleuser => "${mailuser:-$user}${mailuser:+$iopt}",
	visiblename => "$mailfqdn",
	fullname => "$slashed_arches Build Daemon ($hostname)",
	passive => 1,
	dinstall_runs => 1,
};
EOF
		fi

		cat <<EOF

# don't remove this, Perl needs it:
1;

EOF
	fi | ensure_file "$userhome/.dupload.conf" - buildd "$user"

	if true; then
		cat <<EOF
\$distributions = [
EOF
		for suite in $suites; do
			for a in $arch ${other_arches:-}; do
				cat <<EOF
	{
		dist_name => [ "$suite" ],
		built_architecture => '$a',
		sbuild_chroot => "${suite}-${a}-sbuild",
		wanna_build_ssh_host => "buildd.debian.org",
		wanna_build_ssh_user => "wb-buildd",
		wanna_build_ssh_socket => "buildd.debian.org.ssh",
		wanna_build_ssh_options => [ '-o', 'BatchMode=yes' ],
		wanna_build_db_user => "buildd_$arch-$hostname$hopt",
		dupload_local_queue_dir => "upload",
		no_auto_build => [${no_auto_build:+ $no_auto_build }],
		no_build_regex => "${no_build_regex:+$no_build_regex}",
		weak_no_auto_build => [],
		logs_mailed_to => '$logs_mailed_to',
EOF
				if [ "$suite" = "experimental" -a "$arch" = "alpha" ] || [ "$suite" = "experimental" -a "$arch" = "m68k" ] ; then
					cat <<EOF
		# aptitude currently crashes on m68k when used as the build-dep resolver
		build_dep_resolver => 'aspcud',
EOF
				fi
				if [ "$suite" = "experimental" -a "$arch" = "ia64" ] || [ "$suite" = "experimental" -a "$arch" = "sh4" ] || [ "$suite" = "experimental" -a "$arch" = "x32" ] ; then
					cat <<EOF
		# make sure aptitude is used as the build-dep resolver on ia64, sh4 and x32 for experimental
		build_dep_resolver => 'aptitude',
EOF
				fi
				if [ "$sign_with" != "manual" ]; then
					cat <<EOF
		sign_with => "$sign_with",
EOF
				fi
				cat <<EOF
	},
EOF
			done
		done
		cat <<EOF
];

\$upload_queues = [
	{
		dupload_local_queue_dir => "upload",
EOF
		if [ "$debian_ports" != "no" ]; then
			cat <<EOF
		dupload_archive_name => "dports",
EOF
		else
			cat <<EOF
		dupload_archive_name => "ftp-master",
EOF
		fi
		cat <<EOF
	}
];

my \$autoclean_interval = 86400;
my \$secondary_daemon_threshold = undef;
\$admin_mail = '$admin_mail';
\$statistics_mail = \$admin_mail;

# How many days until archiving build logs
my \$buildd_log_keep = 7;

# Log successful messages from upload queue daemon?
my \$log_queued_messages = 1;

# Send rotated daemon.log files?
\$daemon_log_send = 0;

# Arrakis is a dedicated daemon, it shouldn't need nice
\$nice_level = 10;

my \$max_build = 1;

# Ask whether packages should be built if they failed earlier?
\$should_build_msgs = 0;

# Warn if any files older than this many days are present.
# Defaults to 7, but we clean up any files older than 7 days with a cronjob, so
# sometimes harmless warnings are given. Therefore let's increase this to twice
# the value.
\$warning_age = 14;
EOF
	if [ "$arch" = "kfreebsd-amd64" ]; then
		cat <<EOF

# wanna-build is happy to schedule lots of KDE/Qt builds, but apt doesn't want
# to install some things like libkf5kio-dev; let's not let this block
# non-KDE/Qt packages.
\$max_sbuild_fails = 99;
EOF
	fi
	cat <<EOF

# Don't remove this, Perl needs it
1;
EOF
	fi | ensure_file "$userhome/.builddrc" - buildd "$user" 644 "$userhome/EXIT-DAEMON-PLEASE"

	err=0 && getent group sbuild >/dev/null || err=$?
	if [ $err -eq 2 ]; then
		die "sbuild group does not yet exist"
	elif [ $err -ne 0 ]; then
		die "getent group sbuild: exit code $err"
	fi
	if ! getent group sbuild | grep "\<$user\>" > /dev/null; then
		PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/sbin:/usr/local/bin run adduser $user sbuild
	fi
done

if true; then
	cat <<EOF1
#!/bin/bash
# give a year (e.g. 2022) as first parameter to generate key which expires Dec 31 on that year
set -e
set -u
set -o pipefail
if [ -z "\${1-}" ]; then
	ENDDATE=1y
else
	ENDDATE=\${1:0:4}1231T000000
fi
if [ -e /usr/bin/gpg2 ]; then
	GPG=gpg2
	GPG_PKG=gnupg2
else
	GPG=gpg
	GPG_PKG=gnupg
fi
GPG_OPTS="--no-options"
# GnuPG pre-2.1 doesn't support (nor does it need) --pinentry-mode loopback
# Get the version outside the if so we die early and gracefully if gnupg isn't installed
GPG_VER=\`dpkg-query -W -f '\${Version}' \$GPG_PKG\`
if dpkg --compare-versions \$GPG_VER ge 2.1; then
	GPG_OPTS="\$GPG_OPTS --pinentry-mode loopback"
fi
GPG_OPTS="\$GPG_OPTS --passphrase-file /dev/null --no-default-keyring"
echo "Generating key..."
su buildd -c "\$GPG \$GPG_OPTS --batch --gen-key --cert-digest-algo SHA512 --homedir ~buildd/.gnupg" <<EOF
Key-Type: RSA
Key-Usage: sign
Key-Length: 4096
Name-Real: Debian buildd autosigning key for $hostname
Name-Email: buildd_$arch-$hostname@buildd.debian.org
Expire-Date: \$ENDDATE
%commit
EOF
for i in \`seq 2 $num_users\`; do
	[ \$i -eq 1 ] && iopt="" || iopt="\$i"
	user=buildd\$iopt
	echo "Copying to ~\$user/.gnupg..."
	su buildd -c "\$GPG \$GPG_OPTS --homedir ~buildd/.gnupg --export-secret-keys" | \
		su \$user -c "\$GPG \$GPG_OPTS --import --homedir ~\$user/.gnupg"
done
KEY=\$(su buildd -c "\$GPG \$GPG_OPTS -k | tail -n 3 | head -n 1 | xargs")
PUBKEYFILE=~buildd/gpg-public-key-\$(hostname -s)-\$KEY
su buildd -c "\$GPG \$GPG_OPTS --homedir ~buildd/.gnupg --export \$KEY > \$PUBKEYFILE"
echo "Done - exported public key \$KEY to \$PUBKEYFILE"
EOF1
fi | ensure_file "/usr/local/sbin/gpg-new-buildd-key" - root root 555

# +====================================================================+
# | Setup cron                                                         |
# +====================================================================+

if true; then
	cat <<EOF
# crontab for dsa-puppet
MAILTO=$admin_mail
# min        hour         day mon wday user     cmd
0            *            *   *   *    root     cd "$puppet_root" && chronic git fetch && chronic git reset --hard origin/master && cd ~root && "$puppet_root/scripts/setup-buildd"
EOF
	if [ "$enable_notify_monitor" != 'no' ]; then
		cat <<EOF
*/5          *            *   *   *    root     "$puppet_root/scripts/notify-monitor"
EOF
	fi
fi | ensure_file /etc/cron.d/dsa-puppet - root root

if true; then
	cat <<EOF
# crontab for buildd
MAILTO=$admin_mail
# min        hour         day mon wday user     cmd
EOF
	if [ "$arch" = "powerpcspe" ]; then
		cat <<EOF
13           21           *   *   0    root     PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/sbin:/usr/local/bin setup-all-dchroots buildd
EOF
	else
		cat <<EOF
13           21           *   *   0,3  root     PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/sbin:/usr/local/bin setup-all-dchroots buildd
EOF
	fi
	cat <<EOF
*/5          *            *   *   *    root     if [ -f ~root/SETUP-DCHROOTS-PLEASE ]; then rm ~root/SETUP-DCHROOTS-PLEASE && PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/sbin:/usr/local/bin setup-all-dchroots buildd; fi

EOF
	for i in `seq 1 $num_users`; do
		[ $i -eq 1 ] && iopt="" || iopt="$i"
		user=buildd$iopt
		cat <<EOF
10,25,40,55  *            *   *   *    $user   /usr/bin/buildd-uploader
$((4+i)),$((19+i)),$((34+i)),$((49+i))   *            *   *   *    $user   /usr/bin/buildd-watcher
@daily                                 $user   [ -d ~$user/logs/ ]            && find ~$user/logs/            -type f -mtime +7 -delete
EOF
		if [ "$auto_clean_uploads" != 'no' ]; then
			cat <<EOF
@daily                                 $user   [ -d ~$user/upload/ ]          && find ~$user/upload/          -type f -mtime +7 -delete
@daily                                 $user   [ -d ~$user/upload-security/ ] && find ~$user/upload-security/ -type f -mtime +7 -delete
EOF
		fi
	done
fi | ensure_file /etc/cron.d/buildd - root root
